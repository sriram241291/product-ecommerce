import { Injectable } from '@angular/core';
import { GlobalModel } from './global.model';
import { Observable, Subject } from 'rxjs';

@Injectable()

export class GlobalService {
    snackbarMsg = new Subject<[boolean, string]>();
    products: GlobalModel[];
    constructor() {
        this.products = [
            {
                id: "1",
                brand: 'Apple',
                name: "Apple iPhone 11 (64GB) - White",
                description: "All about Iphone",
                price: 64900,
            },
            {
                id: "2",
                brand: 'Samsung',
                name: "Samsung S9 (64GB) - White",
                description: "All about Samsung",
                price: 64900,
            },
            {
                id: "3",
                brand: 'Apple',
                name: "Apple iPhone 10 (64GB) - White",
                description: "All about Iphone",
                price: 64900,
            },
            {
                id: "4",
                brand: 'Apple',
                name: "Apple iPhone 8 plus(64GB) - White",
                description: "All about Iphone",
                price: 64900,
            },
            {
                id: "5",
                brand: 'Apple',
                name: "Apple iPhone 11 (64GB) - White",
                description: "All about Iphone",
                price: 64900,
            },
            {
                id: "6",
                brand: 'Apple',
                name: "Apple iPhone 11 (64GB) - White",
                description: "All about Iphone",
                price: 64900,
            }
        ]
    }
    showSnackBar(status: boolean, msg: string) {
        this.snackbarMsg.next([status, msg]);
    }

    getSnackbarMsg(): Observable<[boolean, string]> {
        return this.snackbarMsg
    }
}