import { Component, OnInit } from '@angular/core';
import { GlobalModel } from '../global.model';
import { GlobalService } from '../global.service';

@Component({
	selector: 'app-product-listing',
	templateUrl: './product-listing.component.html',
	styleUrls: ['./product-listing.component.scss']
})
export class ProductListingComponent implements OnInit {
	products : any;
	constructor(private _globalservice: GlobalService) { }
	ngOnInit() {
		this.products = this._globalservice.products;
	}

}
